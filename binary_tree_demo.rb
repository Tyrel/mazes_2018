require "grid"
require "binary_tree"

grid = Grid.new(6, 10)
BinaryTree.on(grid)

puts grid
img = grid.to_png
img.save "bt.png"