# Tyrel's Edition of Mazes for Programmers


Bundle insall to get ChunkyPNG, then run one of the demos.

```bash
$ bundle install
$ ruby -I. binary_tree_demo.rb
$ ruby -I. XXX_demo.rb  # Etc...
```

# What??

Following along with this book [https://pragprog.com/book/jbmaze/mazes-for-programmers](https://pragprog.com/book/jbmaze/mazes-for-programmers)

## Mazes included

- BinaryTree binary_tree.rb  Chapter 2
- Sidewinder sidewinder.rb  Chapter 2